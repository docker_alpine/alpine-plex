# alpine-plex
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-plex)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-plex)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-plex/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-plex/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [Plex Media Server](https://www.plex.tv/)
    - The Plex Media Server either running on Windows, macOS, Linux, FreeBSD or a NAS which organizes audio (music) and visual (photos and videos) content from personal media libraries and streams it to their player counterparts.



----------------------------------------
#### Run

```sh
docker run -d \
           --net=host \
		   -p 32400:32400/tcp \
		   -p 3005:3005/tcp \
		   -p 8324:8324/tcp \
		   -p 32469:32469/tcp \
		   -p 1900:1900/udp \
		   -p 32410:32410/udp \
		   -p 32412:32412/udp \
		   -p 32413:32413/udp \
		   -p 32414:32414/udp \
		   -v /comf.d:/conf.d \
		   -v /transcode:/transcode \
		   -v /data:/data \
		   forumi0721/alpine-plex:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:32400/](http://localhost:32400/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| --net=host         | for Broadcast                                    |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 32400/tcp          | Access port                                      |
| 3005/tcp           | Plex port                                        |
| 8324/tcp           | Plex port                                        |
| 32469/tcp          | Plex port                                        |
| 1900/udp           | Plex port                                        |
| 32410/udp          | Plex port                                        |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |
| /data              | Media data                                       |
| /transcode         | Transcode data                                   |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

