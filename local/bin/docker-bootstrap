#!/bin/sh

set -e

if [ -e /.init ]; then
	exit
fi

touch /.init

mkdir -p /conf.d/plex /transcode

RUN_USER_NAME=${RUN_USER_NAME:-forumi0721}
RUN_USER_UID=${RUN_USER_UID:-1000}
RUN_USER_GID=${RUN_USER_GID:-100}
RUN_USER_GID_LIST=${RUN_USER_GID_LIST:-audio,video}

PLEX_CLAIM=${PLEX_CLAIM}
ADVERTISE_IP=${ADVERTISE_IP}
ALLOWED_NETWORKS=${ALLOWED_NETWORKS:-0.0.0.0/0.0.0.0}

docker-adduser -u ${RUN_USER_UID} -g ${RUN_USER_GID} -G ${RUN_USER_GID_LIST} -H /dev/null -S /sbin/nologin -R ${RUN_USER_NAME}

docker-chown ${RUN_USER_NAME} /conf.d/plex
docker-chown ${RUN_USER_NAME} /transcode

prefFile="/conf.d/plex/Library/Application Support/Plex Media Server/Preferences.xml"

getPref() {
	local key="${1}"

	xmlstarlet sel -T -t -m "/Preferences" -v "@${key}" -n "${prefFile}"
}

setPref() {
	local key="${1}"
	local value="${2}"

	local count="$(xmlstarlet sel -t -v "count(/Preferences/@${key})" "${prefFile}")"
	count=$((${count} + 0))
	if [[ ${count} -gt 0 ]]; then
		xmlstarlet ed --inplace --update "/Preferences/@${key}" -v "${value}" "${prefFile}"
	else
		xmlstarlet ed --inplace --insert "/Preferences" --type attr -n "${key}" -v "${value}" "${prefFile}"
	fi
}

if [ ! -s "${prefFile}" ]; then
	echo "Creating pref shell"
	mkdir -p "$(dirname "${prefFile}")"
	cat > "${prefFile}" <<-EOF
	<?xml version="1.0" encoding="utf-8"?>
	<Preferences/>
	EOF

	serial="$(getPref "MachineIdentifier")"
	if [ -z "${serial}" ]; then
		serial="$(uuidgen)"
		setPref "MachineIdentifier" "${serial}"
	fi
	clientId="$(getPref "ProcessedMachineIdentifier")"
	if [ -z "${clientId}" ]; then
		clientId="$(echo -n "${serial}- Plex Media Server" | sha1sum | cut -b 1-40)"
		setPref "ProcessedMachineIdentifier" "${clientId}"
	fi

	# Get server token and only turn claim token into server token if we have former but not latter.
	token="$(getPref "PlexOnlineToken")"
	if [ ! -z "${PLEX_CLAIM}" ] && [ -z "${token}" ]; then
		echo "Attempting to obtain server token from claim token"
		loginInfo="$(curl -X POST \
			-H 'X-Plex-Client-Identifier: '${clientId} \
			-H 'X-Plex-Product: Plex Media Server'\
			-H 'X-Plex-Version: 1.1' \
			-H 'X-Plex-Provides: server' \
			-H 'X-Plex-Platform: Linux' \
			-H 'X-Plex-Platform-Version: 1.0' \
			-H 'X-Plex-Device-Name: PlexMediaServer' \
			-H 'X-Plex-Device: Linux' \
			"https://plex.tv/api/claim/exchange?token=${PLEX_CLAIM}")"
		token="$(echo "$loginInfo" | sed -n 's/.*<authentication-token>\(.*\)<\/authentication-token>.*/\1/p')"

		if [ "$token" ]; then
			echo "Token obtained successfully"
			setPref "PlexOnlineToken" "${token}"
		fi
	fi

	if [ ! -z "${ADVERTISE_IP}" ]; then
		setPref "customConnections" "${ADVERTISE_IP}"
	fi

	if [ ! -z "${ALLOWED_NETWORKS}" ]; then
		setPref "allowedNetworks" "${ALLOWED_NETWORKS}"
	fi

	# Set transcoder temp if not yet set
	if [ -z "$(getPref "TranscoderTempDirectory")" ]; then
		setPref "TranscoderTempDirectory" "/transcode"
	fi

	# Disable IPv6
	setPref "EnableIPv6" "0"

	echo "Plex Media Server first run setup complete"
fi

