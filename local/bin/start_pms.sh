#!/bin/sh

#set -e

RUN_USER_NAME="${RUN_USER_NAME:-root}"
RUN_USER_GROUP="$(id -gn "${RUN_USER_NAME}")"

echo "${RUN_USER_NAME}.${RUN_USER_GROUP}"

export PLEX_MEDIA_SERVER_MAX_PLUGIN_PROCS=6
export PLEX_MEDIA_SERVER_HOME=/app/plexmediaserver
export PLEX_MEDIA_SERVER_LIB=${PLEX_MEDIA_SERVER_HOME}/lib
export PLEX_MEDIA_SERVER_MAX_STACK_SIZE=3000
export PLEX_MEDIA_SERVER_TMPDIR=/tmp
export PLEX_MEDIA_SERVER_APPLICATION_SUPPORT_DIR="/conf.d/plex/Library/Application Support"
export PLEX_MEDIA_SERVER_USER=${RUN_USER_NAME}

export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

export LD_LIBRARY_PATH=${PLEX_MEDIA_SERVER_LIB}:${LD_LIBRARY_PATH}:/lib:/usr/lib
export TMPDIR="${PLEX_MEDIA_SERVER_TMPDIR}"

echo $PLEX_MEDIA_SERVER_MAX_PLUGIN_PROCS $PLEX_MEDIA_SERVER_MAX_STACK_SIZE $PLEX_MEDIA_SERVER_APPLICATION_SUPPORT_DIR

mkdir -p /conf.d/plex /transcode "${PLEX_MEDIA_SERVER_APPLICATION_SUPPORT_DIR}"
docker-chown ${RUN_USER_NAME} /conf.d/plex
docker-chown ${RUN_USER_NAME} /transcode
docker-chown ${RUN_USER_NAME} "${PLEX_MEDIA_SERVER_APPLICATION_SUPPORT_DIR}"

ulimit -s $PLEX_MAX_STACK_SIZE

sleep 3

pkill Plex\ Media\ Server || true

# Add sleep - Possible fix for start on boot.
sleep 5

cd /app/plexmediaserver
su-exec ${RUN_USER_NAME} ./Plex\ Media\ Server

